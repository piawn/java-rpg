# RPG Collaboration
The game is designed to be an infinitely scaling tower 
where a party of 4 heroes will climb the tower and defeat enemies!

### Status
The only functionality available is a console program where
you can test characters attack damage, make them take damage and
change their weapons and spells.
