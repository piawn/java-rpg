package main.java.characters.caster;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.categories.CasterHero;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.DamagingSpell;

/*
 Mages are masters of arcane magic. Their skills are honed after years of dedicated study.
 They conjure arcane energy to deal large amounts of damage to enemies.
 They are vulnerable to physical attacks but are resistant to magic.
*/
public class Mage extends CasterHero {

    public Mage(DamagingSpell damagingSpell) {
        super(CharacterCategory.Caster,
                ArmorType.Cloth,
                WeaponCategory.MagicWeapon,
                CharacterBaseStatsDefensive.MAGE_BASE_HEALTH,
                CharacterBaseStatsDefensive.MAGE_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.MAGE_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.MAGE_MAGIC_POWER,
                damagingSpell);
    }

    @Override
    public String getName() {
        return "Mage";
    }
}
