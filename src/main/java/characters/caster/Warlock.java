package main.java.characters.caster;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.categories.CasterHero;
import main.java.characters.abstractions.CharacterCategory;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.DamagingSpell;

/*
 Warlocks are masters of chaos, entropy, and death. They were once mages but were corrupted by power.
 They can conjure up pure chaos energy to destroy their enemies.
*/
public class Warlock extends CasterHero {

    public Warlock(DamagingSpell damagingSpell) {
        super(CharacterCategory.Caster,
                ArmorType.Cloth,
                WeaponCategory.MagicWeapon,
                CharacterBaseStatsDefensive.WARLOCK_BASE_HEALTH,
                CharacterBaseStatsDefensive.WARLOCK_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.WARLOCK_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.WARLOCK_MAGIC_POWER,
                damagingSpell);
    }

    @Override
    public String getName() {
        return "Warlock";
    }
}
