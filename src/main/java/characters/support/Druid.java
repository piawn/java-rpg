package main.java.characters.support;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.HealerSupport;
import main.java.characters.abstractions.categories.SupportHero;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.HealingSpell;
import main.java.spells.abstractions.Spell;

/*
 Class description:
 ------------------
 Druids are spell casters who use nature based magic to aid their allies in battle.
 They can heal their allies or protect them using the forces of nature.
 As a support class they only have defensive stats.
*/
public class Druid extends SupportHero implements HealerSupport {

    protected HealingSpell healingSpell;
    protected MagicWeapon magicWeapon;

    public Druid(HealingSpell healingSpell) {
        super(CharacterCategory.Support, ArmorType.Leather, WeaponCategory.MagicWeapon,
                CharacterBaseStatsDefensive.DRUID_BASE_HEALTH,
                CharacterBaseStatsDefensive.DRUID_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.DRUID_BASE_MAGIC_RES);
        this.healingSpell = healingSpell;
    }

    // Returns how much healing was done based on magic weapon and healingspell
    public double healPartyMember(){
        this.magicWeapon = (MagicWeapon) getWeapon();
        double healing = this.healingSpell.getHealingAmount() * this.magicWeapon.getMagicPowerModifier() * this.magicWeapon.getRarity();
        return healing;
    }

    @Override
    public boolean setSpell(Spell spell) {
        if (spell instanceof HealingSpell){
            this.healingSpell = (HealingSpell) spell;
            return true;
        }
        return false;
    }

    @Override
    public Spell getSpell() {
        return this.healingSpell;
    }

    @Override
    public String getName() {
        return "Druid";
    }
}
