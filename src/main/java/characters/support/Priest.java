package main.java.characters.support;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.ShieldSupport;
import main.java.characters.abstractions.categories.SupportHero;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.ShieldingSpell;
import main.java.spells.abstractions.Spell;

/*
 Priest are the servants of the light and goodness.
 They use holy magic to heal and shield allies.
 As a support class they only have defensive stats.
*/
public class Priest extends SupportHero implements ShieldSupport {

    private ShieldingSpell shieldingSpell;
    protected MagicWeapon magicWeapon;

    public Priest(ShieldingSpell shieldingSpell) {
        super(CharacterCategory.Support, ArmorType.Cloth, WeaponCategory.MagicWeapon,
                CharacterBaseStatsDefensive.PRIEST_BASE_HEALTH,
                CharacterBaseStatsDefensive.PRIEST_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.PRIEST_BASE_MAGIC_RES);
        this.shieldingSpell = shieldingSpell;
    }

    // Return how much shielding was done based on party members currentMaxHealth
    public double shieldPartyMember(double partyMemberMaxHealth) {
        this.magicWeapon = (MagicWeapon) this.weapon;
        return (partyMemberMaxHealth * this.shieldingSpell.getAbsorbShieldPercentage()) + (this.magicWeapon.getMagicPowerModifier() + this.magicWeapon.getRarity()); // Return calculated shield value
    }

    @Override
    public boolean setSpell(Spell spell) {
        if (spell instanceof ShieldingSpell){
            this.shieldingSpell = (ShieldingSpell) spell;
            return true;
        }
        return false;
    }

    @Override
    public Spell getSpell() {
        return this.shieldingSpell;
    }

    @Override
    public String getName() {
        return "Priest";
    }
}
