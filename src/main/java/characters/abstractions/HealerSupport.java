package main.java.characters.abstractions;

public interface HealerSupport {
    double healPartyMember();
}
