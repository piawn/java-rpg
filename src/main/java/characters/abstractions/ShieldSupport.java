package main.java.characters.abstractions;

public interface ShieldSupport {
    double shieldPartyMember(double partyMemberMaxHealth);
}
