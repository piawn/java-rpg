package main.java.characters.abstractions;

import main.java.spells.abstractions.Spell;

public interface MagicHero {
    boolean setSpell(Spell spell);
    Spell getSpell();
}
