package main.java.characters.abstractions;

import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponCategory;

public abstract class Hero {
    protected CharacterCategory CHARACTER_CAT;
    protected ArmorType ARMOR_TYPE;
    protected WeaponCategory WEAPON_CATEGORY;

    // Base stats defensive
    public double baseHealth;
    protected double basePhysReductionPercent; // Armor
    protected double baseMagicReductionPercent; // Magic armor

    public Weapon weapon;
    public Armor armor;

    // Active trackers and flags
    protected double currentMaxHealth;
    protected double currentHealth;
    protected boolean isDead = false;

    public Hero(CharacterCategory charCat, ArmorType armortype, WeaponCategory weaponCat, double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent){
        this.CHARACTER_CAT = charCat;
        this.ARMOR_TYPE = armortype;
        this.WEAPON_CATEGORY = weaponCat;
        this.baseHealth = baseHealth;
        this.basePhysReductionPercent = basePhysReductionPercent;
        this.baseMagicReductionPercent = baseMagicReductionPercent;
        this.currentHealth = baseHealth;
    }

    // Public getters statuses and stats
    public double getCurrentMaxHealth(){
        this.currentMaxHealth = baseHealth + armor.getHealthModifier() + armor.getRarityModifier();
        return this.currentMaxHealth;
    }

    public Armor getArmor(){
        return this.armor;
    }

    public Weapon getWeapon(){
        return this.weapon;
    }

    public double getCurrentHealth() {
        return currentHealth;
    }

    public boolean getDead() {
        return isDead;
    }

    // EQUIPMENT BEHAVIOR
    public boolean equipArmor(Armor armor) {
        if (ARMOR_TYPE == armor.getArmorType()){
            this.armor = armor;
            return true;
        }
        return false;
    }

    public boolean equipWeapon(Weapon weapon) {
        if (WEAPON_CATEGORY == weapon.getWeaponCategory()){
            this.weapon = weapon;
            return true;
        }
        return false;
    }

    // CHARACTER BEHAVIOR
    // Returns damage taken based on incoming damage, weapon rarity and type of damage
    public double takeDamage(double incomingDamage , String damageType) {
        double damageTaken = 0;
        if (damageType.equals("physical")){
            damageTaken = incomingDamage * (1-(this.basePhysReductionPercent * this.armor.getPhysRedModifier() * this.armor.getRarityModifier()));
        } else if (damageType.equals("magic")){
            damageTaken = incomingDamage * (1-(this.baseMagicReductionPercent * this.armor.getMagicRedModifier() * this.armor.getRarityModifier()));
        }
        if (damageTaken < 0){
            damageTaken = 1;
        }
        return damageTaken;
    }


    public void setHealthAfterHealing(double healing){
        this.currentHealth += healing;
    }

    public abstract String getName();
}
