package main.java.characters.abstractions.categories;

import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.Hero;
import main.java.characters.abstractions.MagicHero;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.spells.abstractions.DamagingSpell;
import main.java.spells.abstractions.Spell;

public abstract class CasterHero extends Hero implements MagicHero {

    protected DamagingSpell damagingSpell;
    protected MagicWeapon magicWeapon;

    protected double baseMagicPower;

    public CasterHero(CharacterCategory charCat, ArmorType armortype, WeaponCategory weaponCat, double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, double baseMagicPower, DamagingSpell damagingSpell) {
        super(charCat, armortype, weaponCat, baseHealth, basePhysReductionPercent, baseMagicReductionPercent);
        this.baseMagicPower = baseMagicPower;
        this.damagingSpell = damagingSpell;
    }

    // Return damage based on its spells and magic weapon
    public double castDamagingSpell(){
        magicWeapon = (MagicWeapon) this.weapon;
        return this.baseMagicPower * this.magicWeapon.getMagicPowerModifier() * this.damagingSpell.getSpellDamageModifier() * this.magicWeapon.getRarity();
    }

    @Override
    public boolean setSpell(Spell spell) {
        if (spell instanceof DamagingSpell){
            this.damagingSpell = (DamagingSpell) spell;
            return true;
        }
        return false;
    }

    @Override
    public Spell getSpell(){
        return this.damagingSpell;
    }

}
