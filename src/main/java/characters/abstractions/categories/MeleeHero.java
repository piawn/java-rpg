package main.java.characters.abstractions.categories;

import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.Hero;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.MeleeWeapon;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.abstractions.WeaponType;

public abstract class MeleeHero extends Hero {

    protected double baseAttackPower;
    protected MeleeWeapon meleeWeapon;

    public MeleeHero(CharacterCategory charCat, ArmorType armortype, WeaponCategory weaponCat, double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, double baseAttackPower) {
        super(charCat, armortype, weaponCat, baseHealth, basePhysReductionPercent, baseMagicReductionPercent);
        this.baseAttackPower = baseAttackPower;
    }

    // Returns damage based on weapon and attack power
    public double meleeAttack(){
        this.meleeWeapon = (MeleeWeapon) this.weapon;
        double attackDamage = this.baseAttackPower * this.meleeWeapon.getAttackPowerModifier() * meleeWeapon.getRarity();
        return attackDamage;
    }
}
