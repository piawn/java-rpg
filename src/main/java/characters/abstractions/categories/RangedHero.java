package main.java.characters.abstractions.categories;

import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.Hero;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.RangedWeapon;
import main.java.items.weapons.abstractions.WeaponCategory;

public abstract class RangedHero extends Hero {

    protected double baseAttackPower;
    protected RangedWeapon rangedWeapon;


    public RangedHero(CharacterCategory charCat, ArmorType armortype, WeaponCategory weaponCat, double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent, double baseAttackPower) {
        super(charCat, armortype, weaponCat, baseHealth, basePhysReductionPercent, baseMagicReductionPercent);
        this.baseAttackPower = baseAttackPower;
    }

    // Returns dealt damage based on weapon and attack power
    public double rangedAttack(){
        this.rangedWeapon = (RangedWeapon) this.weapon;
        double attackDamage = this.baseAttackPower * this.rangedWeapon.getAttackPowerModifier() * rangedWeapon.getRarity();
        return attackDamage;
    }
}
