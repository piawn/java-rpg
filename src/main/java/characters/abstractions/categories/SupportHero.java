package main.java.characters.abstractions.categories;

import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.Hero;
import main.java.characters.abstractions.MagicHero;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;

public abstract class SupportHero extends Hero implements MagicHero {

    public SupportHero(CharacterCategory charCat, ArmorType armortype, WeaponCategory weaponCat, double baseHealth, double basePhysReductionPercent, double baseMagicReductionPercent) {
        super(charCat, armortype, weaponCat, baseHealth, basePhysReductionPercent, baseMagicReductionPercent);
    }


}
