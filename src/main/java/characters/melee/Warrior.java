package main.java.characters.melee;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.categories.MeleeHero;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
/*
 Warriors are combat veterans, durable forces on the battlefield.
 They are masters of the blade and wield it with unmatched ferocity.
*/
public class Warrior extends MeleeHero {

    public Warrior() {
        super(CharacterCategory.Melee,
                ArmorType.Plate,
                WeaponCategory.BladeWeapon,
                CharacterBaseStatsDefensive.WARRIOR_BASE_HEALTH,
                CharacterBaseStatsDefensive.WARRIOR_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.WARRIOR_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.WARRIOR_MELEE_ATTACK_POWER);
    }

    @Override
    public String getName() {
        return "Warrior";
    }
}
