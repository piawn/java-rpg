package main.java.characters.melee;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.categories.MeleeHero;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
/*
 Paladins are faithful servants of the light and everything holy.
 They dispatch justice and are filled with vengeance for the wrongs done to society by evil.
 Paladins are very durable and typically wield heavy weapons.
*/
public class Paladin extends MeleeHero {

    public Paladin() {
        super(CharacterCategory.Melee,
                ArmorType.Plate,
                WeaponCategory.BluntWeapon,
                CharacterBaseStatsDefensive.PALADIN_BASE_HEALTH,
                CharacterBaseStatsDefensive.PALADIN_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.PALADIN_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.PALADIN_MELEE_ATTACK_POWER);
    }

    @Override
    public String getName() {
        return "Paladin";
    }
}
