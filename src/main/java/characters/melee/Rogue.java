package main.java.characters.melee;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.categories.MeleeHero;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
/*
 Rogues are stealthy combatants of the shadows.
 They wield bladed weapons with great agility and dispatch their enemies swiftly.
*/
public class Rogue extends MeleeHero {

    public Rogue() {
        super(CharacterCategory.Melee,
                ArmorType.Leather,
                WeaponCategory.BladeWeapon,
                CharacterBaseStatsDefensive.ROGUE_BASE_HEALTH,
                CharacterBaseStatsDefensive.ROGUE_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.ROGUE_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.ROGUE_MELEE_ATTACK_POWER);
    }

    @Override
    public String getName() {
        return "Rogue";
    }
}
