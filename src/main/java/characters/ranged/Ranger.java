package main.java.characters.ranged;
// Imports
import main.java.basestats.CharacterBaseStatsDefensive;
import main.java.basestats.CharacterBaseStatsOffensive;
import main.java.characters.abstractions.CharacterCategory;
import main.java.characters.abstractions.categories.RangedHero;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.WeaponCategory;
/*
 Rangers
 Are masters of ranged combat. They use a wide arsenal of weapons to dispatch enemies.
*/
public class Ranger extends RangedHero {

    public Ranger() {
        super(CharacterCategory.Ranged,
                ArmorType.Mail,
                WeaponCategory.RangedWeapon,
                CharacterBaseStatsDefensive.RANGER_BASE_HEALTH,
                CharacterBaseStatsDefensive.RANGER_BASE_PHYS_RED,
                CharacterBaseStatsDefensive.RANGER_BASE_MAGIC_RES,
                CharacterBaseStatsOffensive.RANGER_RANGED_ATTACK_POWER);
    }

    @Override
    public String getName() {
        return "Ranger";
    }
}
