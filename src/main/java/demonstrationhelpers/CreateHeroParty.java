package main.java.demonstrationhelpers;

import main.java.basestats.ItemRarityModifiers;
import main.java.characters.abstractions.*;
import main.java.characters.abstractions.categories.CasterHero;
import main.java.characters.abstractions.categories.MeleeHero;
import main.java.characters.abstractions.categories.RangedHero;
import main.java.characters.caster.Mage;
import main.java.characters.caster.Warlock;
import main.java.characters.melee.Paladin;
import main.java.characters.melee.Rogue;
import main.java.characters.melee.Warrior;
import main.java.characters.ranged.Ranger;
import main.java.characters.support.Druid;
import main.java.characters.support.Priest;
import main.java.consolehelpers.Color;
import main.java.factories.ArmorFactory;
import main.java.factories.CharacterFactory;
import main.java.factories.SpellFactory;
import main.java.factories.WeaponFactory;
import main.java.items.armor.Cloth;
import main.java.items.armor.Leather;
import main.java.items.armor.Mail;
import main.java.items.armor.Plate;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;
import main.java.items.weapons.abstractions.*;
import main.java.items.weapons.magic.Staff;
import main.java.items.weapons.magic.Wand;
import main.java.items.weapons.melee.bladed.Axe;
import main.java.items.weapons.melee.bladed.Dagger;
import main.java.items.weapons.melee.bladed.Sword;
import main.java.items.weapons.melee.blunt.Hammer;
import main.java.items.weapons.melee.blunt.Mace;
import main.java.items.weapons.ranged.Bow;
import main.java.items.weapons.ranged.Crossbow;
import main.java.items.weapons.ranged.Gun;
import main.java.spells.abstractions.Spell;
import main.java.spells.abstractions.SpellType;
import main.java.spells.damaging.ArcaneMissile;
import main.java.spells.damaging.ChaosBolt;
import main.java.spells.healing.Regrowth;
import main.java.spells.healing.SwiftMend;
import main.java.spells.shielding.Barrier;
import main.java.spells.shielding.Rapture;

import java.util.ArrayList;
import java.util.Scanner;

public class CreateHeroParty {

    CharacterFactory characterFactory = new CharacterFactory();
    ArmorFactory armorFactory = new ArmorFactory();
    WeaponFactory weaponFactory = new WeaponFactory();
    SpellFactory spellFactory = new SpellFactory();
    Scanner sc = new Scanner(System.in);

    ArrayList<Armor> armor = new ArrayList<>();
    ArrayList<Weapon> weapons = new ArrayList<>();
    ArrayList<Spell> spells = new ArrayList<>();
    ArrayList<Hero> characters = new ArrayList<>();

    public void testCharacters(){
        initCharacters();
        startMenu();
    }

    public void startMenu(){
        int option = 0;
        while (option != -1){
            System.out.println("*** RPG CHARACTER TEST ***");
            System.out.println("1 - Make characters attack, shield and heal");
            System.out.println("2 - Make characters take damage");
            System.out.println("3 - Change spell, weapon and rarity for character (default)");
            System.out.println("4 - Show all characters");
            System.out.println("Press 'q' to quit.");

            option = waitForInput();


            switch (option) {
                case 1 -> makeCharactersAttack();
                case 2 -> makeCharactersTakeDamage();
                case 3 -> changeWeaponsAndSpells();
                case 4 -> showHeroParty();
                case -1 -> closeMenu();
                default -> System.out.println("Please choose a valid option!");
            }
        }
    }

    public void initCharacters(){
        double defaultRarity = ItemRarityModifiers.COMMON_RARITY_MODIFIER;

        // ARMOR
        Cloth cloth = (Cloth) armorFactory.getArmor(ArmorType.Cloth, defaultRarity);
        Leather leather = (Leather) armorFactory.getArmor(ArmorType.Leather, defaultRarity);
        Mail mail = (Mail) armorFactory.getArmor(ArmorType.Mail, defaultRarity);
        Plate plate = (Plate) armorFactory.getArmor(ArmorType.Plate, defaultRarity);
        armor.add(cloth);
        armor.add(leather);
        armor.add(mail);
        armor.add(plate);

        // WEAPONS
        // MAGIC
        Staff staff = (Staff) weaponFactory.getWeapon(WeaponType.Staff, defaultRarity);
        Wand wand = (Wand) weaponFactory.getWeapon(WeaponType.Wand, defaultRarity);
        // MELEE - BLADED
        Axe axe = (Axe) weaponFactory.getWeapon(WeaponType.Axe, defaultRarity);
        Dagger dagger = (Dagger) weaponFactory.getWeapon(WeaponType.Dagger, defaultRarity);
        Sword sword = (Sword) weaponFactory.getWeapon(WeaponType.Sword, defaultRarity);
        // MELEE - BLUNT
        Hammer hammer = (Hammer) weaponFactory.getWeapon(WeaponType.Hammer, defaultRarity);
        Mace mace = (Mace) weaponFactory.getWeapon(WeaponType.Mace, defaultRarity);
        // RANGED
        Bow bow = (Bow) weaponFactory.getWeapon(WeaponType.Bow, defaultRarity);
        Crossbow crossbow = (Crossbow) weaponFactory.getWeapon(WeaponType.Crossbow, defaultRarity);
        Gun gun = (Gun) weaponFactory.getWeapon(WeaponType.Gun, defaultRarity);
        weapons.add(staff);
        weapons.add(wand);
        weapons.add(axe);
        weapons.add(dagger);
        weapons.add(sword);
        weapons.add(hammer);
        weapons.add(mace);
        weapons.add(bow);
        weapons.add(crossbow);
        weapons.add(gun);

        // SPELLS
        // DAMAGING
        ArcaneMissile arcaneMissile = (ArcaneMissile) spellFactory.getSpell(SpellType.ArcaneMissile);
        ChaosBolt chaosBolt = (ChaosBolt) spellFactory.getSpell(SpellType.ChaosBolt);
        // HEALING
        Regrowth regrowth = (Regrowth) spellFactory.getSpell(SpellType.Regrowth);
        SwiftMend swiftMend = (SwiftMend) spellFactory.getSpell(SpellType.SwiftMend);
        // SHIELDING
        Barrier barrier = (Barrier) spellFactory.getSpell(SpellType.Barrier);
        Rapture rapture = (Rapture) spellFactory.getSpell(SpellType.Rapture);

        spells.add(arcaneMissile);
        spells.add(chaosBolt);
        spells.add(regrowth);
        spells.add(swiftMend);
        spells.add(barrier);
        spells.add(rapture);

        // CHARACTERS
        // CASTER
        Mage mage = (Mage) characterFactory.getCharacter(CharacterType.Mage, arcaneMissile);
        Warlock warlock = (Warlock) characterFactory.getCharacter(CharacterType.Warlock, chaosBolt);
        // MELEE
        Paladin paladin = (Paladin) characterFactory.getCharacter(CharacterType.Paladin);
        Rogue rogue = (Rogue) characterFactory.getCharacter(CharacterType.Rogue);
        Warrior warrior = (Warrior) characterFactory.getCharacter(CharacterType.Warrior);
        // RANGED
        Ranger ranger = (Ranger) characterFactory.getCharacter(CharacterType.Ranger);
        // SUPPORT
        Druid druid = (Druid) characterFactory.getCharacter(CharacterType.Druid, regrowth);
        Priest priest = (Priest) characterFactory.getCharacter(CharacterType.Priest, barrier);

        mage.equipArmor(cloth);
        mage.equipWeapon(wand);
        characters.add(mage);

        warlock.equipArmor(cloth);
        warlock.equipWeapon(staff);
        characters.add(warlock);

        paladin.equipArmor(plate);
        paladin.equipWeapon(hammer);
        characters.add(paladin);

        rogue.equipArmor(leather);
        rogue.equipWeapon(axe);
        characters.add(rogue);

        warrior.equipArmor(plate);
        warrior.equipWeapon(dagger);
        characters.add(warrior);

        ranger.equipArmor(mail);
        ranger.equipWeapon(crossbow);
        characters.add(ranger);

        druid.equipArmor(leather);
        druid.equipWeapon(wand);
        characters.add(druid);

        priest.equipArmor(cloth);
        priest.equipWeapon(staff);
        characters.add(priest);
    }

    private void changeWeaponsAndSpells() {
        boolean alreadyChangedWeapon = false;
        boolean alreadyChangedSpell = false;

        for (Hero character : characters){
            System.out.print(character.getName() +  " current weapon is " + Color.RED + character.getWeapon().getWeaponName() + Color.RESET);
            for (Weapon weapon : weapons){
                if (!character.getWeapon().equals(weapon) && !alreadyChangedWeapon){
                    alreadyChangedWeapon = character.equipWeapon(weapon);
                }
            }
            alreadyChangedWeapon = false;
            System.out.println(". Changed weapon to " + Color.GREEN + character.getWeapon().getWeaponName() + Color.RESET + ".\n");

            if (character instanceof MagicHero){
                System.out.print(character.getName() +  " current spell is " + Color.RED + ((MagicHero) character).getSpell().getSpellName() + Color.RESET);
                for (Spell spell : spells){
                    if (!((MagicHero) character).getSpell().equals(spell) && !alreadyChangedSpell){
                        alreadyChangedSpell = ((MagicHero) character).setSpell(spell);
                    }
                }
                System.out.println(". Changed spell to " + Color.GREEN + ((MagicHero) character).getSpell().getSpellName() + Color.RESET + ".\n");
            }
            alreadyChangedSpell = false;
        }
    }

    private int waitForInput(){
        int option = 0;
        while (option == 0) {
            if (sc.hasNextInt()) {
                option = sc.nextInt();
            } else if (sc.hasNext()) {
                String invalidInput = sc.next();
                if (invalidInput.equals("q")) {
                    option = -1;
                } else {
                    System.out.println("Your input '" + invalidInput + "' is not valid.");
                    System.out.println("Only numbers is valid input.");
                }
            }
        }
        return option;
    }

    private void makeCharactersAttack(){
        for (Hero character : characters){
            if (character instanceof CasterHero){
                System.out.println(character.getName() + " casts a spell!");
                System.out.println("The spell dealt " + (int) ((CasterHero) character).castDamagingSpell() + " damage!\n");
            } else if (character instanceof MeleeHero){
                System.out.println(character.getName() + " did a melee attack!");
                System.out.println("The attack dealt " + (int) ((MeleeHero) character).meleeAttack() + " damage!\n");
            } else if (character instanceof RangedHero){
                System.out.println(character.getName() + " did a ranged attack!");
                System.out.println("The attack dealt " + (int) ((RangedHero) character).rangedAttack() + " damage!\n");
            } else if (character instanceof HealerSupport){
                System.out.println(character.getName() + " used a healing spell!");
                System.out.println("The spell healed " + (int) ((HealerSupport) character).healPartyMember() + " health!\n");
            } else if (character instanceof ShieldSupport){
                System.out.println(character.getName() + " used a shielding spell!");
                System.out.println("The spell shielded " + (int) ((ShieldSupport) character).shieldPartyMember(character.getCurrentMaxHealth()) + " damage!\n");
            }
        }
    }

    public void makeCharactersTakeDamage(){
        System.out.println("How much damage should the weapon deal?");
        double incomingDamage = (double) waitForInput();

        for (Hero character : characters){
            System.out.println(character.getName() + " took " +
                    (int) character.takeDamage(incomingDamage,"magic") +
                    " magic damage and " +
                    (int) character.takeDamage(incomingDamage,"physical") +
                    " physical damage!\n" );
        }
    }

    public void showHeroParty(){
        System.out.println("\nAll characters: ");
        for (Hero character : characters){
            System.out.println(character.getName());
        }
        System.out.println();
    }

    private void closeMenu(){
        sc.close();
        System.out.println("Closing program..");
    }
}
