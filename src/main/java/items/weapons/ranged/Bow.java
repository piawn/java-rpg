package main.java.items.weapons.ranged;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.RangedWeapon;
import main.java.items.weapons.abstractions.WeaponCategory;

public class Bow implements RangedWeapon {
    private double attackPowerModifier = WeaponStatsModifiers.BOW_ATTACK_MOD;
    private double rarity;

    public Bow(double rarity) {
        this.rarity = rarity;
    }

    // Public properties
    @Override
    public double getRarity() {
        return rarity;
    }

    @Override
    public double getAttackPowerModifier() {
        return attackPowerModifier;
    }

    @Override
    public String getWeaponName() {
        return "Bow";
    }

    @Override
    public WeaponCategory getWeaponCategory() {
        return WeaponCategory.RangedWeapon;
    }
}
