package main.java.items.weapons.melee.blunt;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BluntWeapon;
import main.java.items.weapons.abstractions.WeaponCategory;

public class Hammer implements BluntWeapon {
    private double attackPowerModifier = WeaponStatsModifiers.HAMMER_ATTACK_MOD;
    private double rarity;

    public Hammer(double rarity) {
        this.rarity = rarity;
    }

    // Public properties
    @Override
    public double getRarity() {
        return rarity;
    }

    @Override
    public double getAttackPowerModifier() {
        return attackPowerModifier;
    }

    @Override
    public String getWeaponName() {
        return "Hammer";
    }

    @Override
    public WeaponCategory getWeaponCategory() {
        return WeaponCategory.BluntWeapon;
    }
}
