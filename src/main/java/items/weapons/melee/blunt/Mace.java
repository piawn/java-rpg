package main.java.items.weapons.melee.blunt;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BluntWeapon;
import main.java.items.weapons.abstractions.WeaponCategory;

public class Mace implements BluntWeapon {
    private double attackPowerModifier = WeaponStatsModifiers.MACE_ATTACK_MOD;
    private double rarity;

    public Mace(double rarity) {
        this.rarity = rarity;
    }

    // Public properties
    @Override
    public double getRarity() {
        return rarity;
    }

    @Override
    public double getAttackPowerModifier() {
        return attackPowerModifier;
    }

    @Override
    public String getWeaponName() {
        return "Mace";
    }

    @Override
    public WeaponCategory getWeaponCategory() {
        return WeaponCategory.BluntWeapon;
    }
}
