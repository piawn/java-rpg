package main.java.items.weapons.melee.bladed;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BladedWeapon;
import main.java.items.weapons.abstractions.WeaponCategory;

public class Dagger implements BladedWeapon {
    private double attackPowerModifier = WeaponStatsModifiers.DAGGER_ATTACK_MOD;
    private double rarity;

    public Dagger(double rarity) {
        this.rarity = rarity;
    }

    // Public properties
    @Override
    public double getRarity() {
        return rarity;
    }

    @Override
    public double getAttackPowerModifier() {
        return attackPowerModifier;
    }

    @Override
    public String getWeaponName() {
        return "Dagger";
    }

    @Override
    public WeaponCategory getWeaponCategory() {
        return WeaponCategory.BladeWeapon;
    }

}
