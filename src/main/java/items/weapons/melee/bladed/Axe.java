package main.java.items.weapons.melee.bladed;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.BladedWeapon;
import main.java.items.weapons.abstractions.WeaponCategory;

public class Axe implements BladedWeapon {
    private double attackPowerModifier = WeaponStatsModifiers.AXE_ATTACK_MOD;
    private double rarity;

    public Axe(double rarity) {
        this.rarity = rarity;
    }

    // Public properties
    @Override
    public double getRarity() {
        return rarity;
    }

    @Override
    public double getAttackPowerModifier() {
        return attackPowerModifier;
    }

    @Override
    public String getWeaponName() {
        return "Axe";
    }

    @Override
    public WeaponCategory getWeaponCategory() {
        return WeaponCategory.BladeWeapon;
    }

}
