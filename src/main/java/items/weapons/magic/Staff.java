package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.abstractions.WeaponType;

public class Staff implements MagicWeapon {
    private double magicPowerModifier = WeaponStatsModifiers.STAFF_MAGIC_MOD;
    private double rarity;

    public Staff(double rarity) {
        this.rarity = rarity;
    }

    // Public properties
    @Override
    public double getRarity() {
        return rarity;
    }

    @Override
    public double getMagicPowerModifier() {
        return magicPowerModifier;
    }

    @Override
    public String getWeaponName() {
        return "Staff";
    }

    @Override
    public WeaponCategory getWeaponCategory() {
        return WeaponCategory.MagicWeapon;
    }
}
