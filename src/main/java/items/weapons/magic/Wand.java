package main.java.items.weapons.magic;

import main.java.basestats.WeaponStatsModifiers;
import main.java.items.weapons.abstractions.MagicWeapon;
import main.java.items.weapons.abstractions.WeaponCategory;
import main.java.items.weapons.abstractions.WeaponType;

public class Wand implements MagicWeapon {
    private double magicPowerModifier = WeaponStatsModifiers.WAND_MAGIC_MOD;
    private double rarity;

    public Wand(double rarity) {
        this.rarity = rarity;
    }

    // Public properties
    @Override
    public double getRarity() {
        return rarity;
    }

    @Override
    public double getMagicPowerModifier() {
        return magicPowerModifier;
    }

    @Override
    public String getWeaponName() {
        return "Wand";
    }

    @Override
    public WeaponCategory getWeaponCategory() {
        return WeaponCategory.MagicWeapon;
    }

}
