package main.java.items.weapons.abstractions;

public enum WeaponCategory {
    MagicWeapon,
    BladeWeapon,
    BluntWeapon,
    RangedWeapon
}
