package main.java.items.weapons.abstractions;

public interface Weapon {
    double getRarity();
    String getWeaponName();

    WeaponCategory getWeaponCategory();
}
