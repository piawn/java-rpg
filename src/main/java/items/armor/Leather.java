package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;

public class Leather extends Armor {

    public Leather(double armorRarity) {
        super(ArmorStatsModifiers.LEATHER_HEALTH_MODIFIER,
                ArmorStatsModifiers.LEATHER_PHYS_RED_MODIFIER,
                ArmorStatsModifiers.LEATHER_MAGIC_RED_MODIFIER,
                armorRarity);
    }

    // Public properties
    @Override
    public String getArmorName() {
        return "Leather";
    }

    @Override
    public ArmorType getArmorType() {
        return ArmorType.Leather;
    }
}
