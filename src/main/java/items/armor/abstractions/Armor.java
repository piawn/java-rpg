package main.java.items.armor.abstractions;

public abstract class Armor {
    protected double healthModifier;
    protected double physRedModifier;
    protected double magicRedModifier;
    protected double rarityModifier;

    public Armor(double healthModifier,
                 double physRedModifier,
                 double magicRedModifier,
                 double armorRarity){
        this.healthModifier = healthModifier;
        this.physRedModifier = physRedModifier;
        this.magicRedModifier = magicRedModifier;
        this.rarityModifier = armorRarity;
    }

    // Public properties
    public abstract String getArmorName();

    public abstract ArmorType getArmorType();

    public double getHealthModifier() {
        return healthModifier;
    }

    public double getPhysRedModifier() {
        return physRedModifier;
    }

    public double getMagicRedModifier() {
        return magicRedModifier;
    }

    public double getRarityModifier() {
        return rarityModifier;
    }
}
