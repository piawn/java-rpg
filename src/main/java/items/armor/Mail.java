package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;

public class Mail extends Armor {

    public Mail(double armorRarity) {
        super(ArmorStatsModifiers.MAIL_HEALTH_MODIFIER,
                ArmorStatsModifiers.MAIL_PHYS_RED_MODIFIER,
                ArmorStatsModifiers.MAIL_MAGIC_RED_MODIFIER,
                armorRarity);
    }

    // Public properties
    @Override
    public String getArmorName() {
        return "Mail";
    }

    @Override
    public ArmorType getArmorType() {
        return ArmorType.Mail;
    }

}
