package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;

public class Cloth extends Armor {

    public Cloth(double armorRarity) {
        super(ArmorStatsModifiers.CLOTH_HEALTH_MODIFIER,
                ArmorStatsModifiers.CLOTH_PHYS_RED_MODIFIER,
                ArmorStatsModifiers.CLOTH_MAGIC_RED_MODIFIER,
                armorRarity);
    }

    // Public properties
    @Override
    public String getArmorName() {
        return "Cloth";
    }

    @Override
    public ArmorType getArmorType() {
        return ArmorType.Cloth;
    }

}
