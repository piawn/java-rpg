package main.java.items.armor;

import main.java.basestats.ArmorStatsModifiers;
import main.java.items.armor.abstractions.Armor;
import main.java.items.armor.abstractions.ArmorType;

public class Plate extends Armor {

    public Plate(double armorRarity) {
        super(ArmorStatsModifiers.PLATE_HEALTH_MODIFIER,
                ArmorStatsModifiers.PLATE_PHYS_RED_MODIFIER,
                ArmorStatsModifiers.PLATE_MAGIC_RED_MODIFIER,
                armorRarity);
    }

    // Public properties
    @Override
    public String getArmorName() {
        return "Plate";
    }

    @Override
    public ArmorType getArmorType() {
        return ArmorType.Plate;
    }

}
