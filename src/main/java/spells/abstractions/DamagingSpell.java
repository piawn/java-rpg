package main.java.spells.abstractions;

public interface DamagingSpell extends Spell {
    double getSpellDamageModifier();
}
