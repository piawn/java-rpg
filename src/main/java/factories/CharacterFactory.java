package main.java.factories;
// Imports
import main.java.characters.abstractions.CharacterType;
import main.java.characters.abstractions.Hero;
import main.java.characters.caster.Mage;
import main.java.characters.caster.Warlock;
import main.java.characters.melee.Paladin;
import main.java.characters.melee.Rogue;
import main.java.characters.melee.Warrior;
import main.java.characters.ranged.Ranger;
import main.java.characters.support.Druid;
import main.java.characters.support.Priest;
import main.java.spells.abstractions.DamagingSpell;
import main.java.spells.abstractions.HealingSpell;
import main.java.spells.abstractions.ShieldingSpell;
import main.java.spells.abstractions.Spell;

/*
 This factory exists to be responsible for creating new enemies.
*/
// TODO Once characters can be made with the right compositions, then implement the factory
// By default Druid, Mage, Priest and Warlocks spell is set to null.
public class CharacterFactory {
    public Hero getCharacter(CharacterType characterType) {
        switch(characterType) {
            case Paladin:
                return new Paladin();
            case Ranger:
                return new Ranger();
            case Rogue:
                return new Rogue();
            case Warrior:
                return new Warrior();
            default:
                return null;
        }
    }

    public Hero getCharacter(CharacterType characterType, Spell spell) {
        switch(characterType) {
            case Druid:
                return new Druid((HealingSpell) spell);
            case Mage:
                return new Mage((DamagingSpell) spell);
            case Priest:
                return new Priest((ShieldingSpell) spell);
            case Warlock:
                return new Warlock((DamagingSpell) spell);
            default:
                return null;
        }
    }
}
