package main.java.factories;

import main.java.items.weapons.abstractions.Weapon;
import main.java.items.weapons.abstractions.WeaponType;
import main.java.items.weapons.melee.bladed.Axe;
import main.java.items.weapons.melee.bladed.Dagger;
import main.java.items.weapons.melee.bladed.Sword;
import main.java.items.weapons.melee.blunt.Hammer;
import main.java.items.weapons.melee.blunt.Mace;
import main.java.items.weapons.magic.*;
import main.java.items.weapons.ranged.*;

/*
 This factory exists to be responsible for creating new enemies.
 Object is replaced with Weapon as a return type when refactored to be good OO design.
*/
public class WeaponFactory {
    public Weapon getWeapon(WeaponType weaponType, double itemRarityModifier) {
        switch(weaponType) {
            case Axe:
                return new Axe(itemRarityModifier);
            case Bow:
                return new Bow(itemRarityModifier);
            case Crossbow:
                return new Crossbow(itemRarityModifier);
            case Dagger:
                return new Dagger(itemRarityModifier);
            case Gun:
                return new Gun(itemRarityModifier);
            case Hammer:
                return new Hammer(itemRarityModifier);
            case Mace:
                return new Mace(itemRarityModifier);
            case Staff:
                return new Staff(itemRarityModifier);
            case Sword:
                return new Sword(itemRarityModifier);
            case Wand:
                return new Wand(itemRarityModifier);
            default:
                return null;
        }
    }
}
