package main.java;

import main.java.consolehelpers.Color;
import main.java.demonstrationhelpers.CreateHeroParty;

public class Main {

    public static void main(String[] args) {
        System.out.println(Color.RED + "RED COLORED" +
                Color.RESET + " NORMAL");

        CreateHeroParty create = new CreateHeroParty();

        create.testCharacters();
    }
}
